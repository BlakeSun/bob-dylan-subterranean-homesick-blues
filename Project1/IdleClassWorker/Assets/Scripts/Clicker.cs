﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Clicker : MonoBehaviour
{
    //Current total money
    public static int count = 0;
    public static int countLevel = 1;
    //How many people you've helped
    public static int peopleCounter = 0;
    //Current total debt
    public static int debtCounter = 1000;


    public AudioClip[] drugSounds;
    public AudioClip[] debtSounds;
    private AudioSource audioSource;

    public Sprite[] drugImages; 

    //============================================================================================================================
    //We will divide all money when displayed by 100, because we want to display cents, but floating point errors prohibit floats.
    //============================================================================================================================

    public void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void Update()
    {

        //Don't need to worry about hiding the buttons if you don't have enough, because they start inactive, and turn inactive after purchase.

        //When you hit 0 debt, and you haven't helped 10 people yet, add a person and increase the debt
        if(debtCounter < 1 /*&& peopleCounter < 10 */)
        {
            peopleCounter += 1;
            //Set the debt to the base amount times the amount of people helped (times 5)
            debtCounter = 1000 * (peopleCounter * 5);
        }
        else
        {
            //This is where the game would end, if it did.
        }

        float cheat = Input.GetAxisRaw("Horizontal");
        if(cheat > 0)
        {
            Count();
        }

    }

    //The function that is called when the main button is clicked. The Main Money Maker
    public void Count()
    {
        count += countLevel;
        //print("Money: " + count);
        playRandomDrugSound();
    }

    public void payOffDebt()
    {
        //If I have more money than I have debt for this level, pay it off entirely
        if (count > debtCounter)
        {
            playRandomDebtSound();
            count -= debtCounter;
            debtCounter = 0;
        }
        else
        {
            debtCounter -= count;
            count = 0;
        }
    }

    public int getCount()
    {
        return count;
    }

    public void addToCount(int added)
    {
        count += added;
    }

    public int getCountLevel()
    {
        return countLevel;
    }

    public void setCountLevel(int lvl)
    {
        countLevel = lvl;
    }

    //============================================================================================
    //Sounds

    private void playRandomDrugSound()
    {
        int index = Random.Range(0, drugSounds.Length);
        audioSource.clip = drugSounds[index];
        audioSource.Play();
    }

    private void playRandomDebtSound()
    {
        int index = Random.Range(0, debtSounds.Length);
        audioSource.clip = debtSounds[index];
        audioSource.Play();
    }

    private int index = 0;

    public void changeImage()
    {
        GetComponent<Image>().sprite = drugImages[index];
        GetComponentInChildren<Text>().enabled = false;
        index++;
    }
}
