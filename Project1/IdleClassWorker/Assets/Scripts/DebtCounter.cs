﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebtCounter : MonoBehaviour
{
    // Start is called before the first frame update

    Text text;




    // Start is called before the first frame update
    void Start()
    {

        text = GetComponent<Text>();


    }

    // Update is called once per frame
    void Update()
    {
        //Sets the text for people helped and current debt
        //Divide by 100.0 because we want it to displays in cents
        text.text = "People Helped: " + Clicker.peopleCounter + "\t\t\tCurrent Debt: $" + (Clicker.debtCounter / 100.0);

    }
}
