﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoneyUI : MonoBehaviour
{

    Text text;



    // Start is called before the first frame update
    void Start()
    {

        text = GetComponent<Text>();


    }

    // Update is called once per frame
    void Update()
    {
        //Sets the text to how much money you have
        //Divide by 100.0 because we want it to displays in cents
        text.text = "Money: $" + (Clicker.count / 100.0);
        
    }
}
