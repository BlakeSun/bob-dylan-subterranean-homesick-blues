﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeManager : MonoBehaviour
{

    //The time for the auto-clicker increments
    public float countTime = .5f;

    public GameObject UpgradeDealerButton;
    public GameObject UpgradeDrugButton;

    //Cost of the manual button upgrades
    public int firstUpgradeCost = 999;
    public int secondUpgradeCost = 2499;
    public int thirdUpgradeCost = 4999;

    //Current amount of Dealers auto selling drugs
    public static int currDealerLevel = 0;
    public int dealerSellValue = 50;

    //Reference to the money of the player
    public Clicker MoneyHolder;

    //Sounds
    public AudioClip[] dealerSounds;
    public AudioClip[] betterDrugSounds; //Even though there's only 1 right now, might as well leave it open for additions
    private AudioSource audioSource;

    public void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    //Makes the button appear for upgrading the manual click
    private void Update()
    {
        if (MoneyHolder.getCountLevel() == 1)
        {
            if (MoneyHolder.getCount() > firstUpgradeCost)
            {
                UpgradeDrugButton.GetComponentInChildren<Text>().text = "Sell Better Drugs \n $10.00";
                UpgradeDrugButton.SetActive(true);
            }
        }
        else if (MoneyHolder.getCountLevel() == 2)
        {
            if (MoneyHolder.getCount() > secondUpgradeCost)
            {
                UpgradeDrugButton.GetComponentInChildren<Text>().text = "Sell Better Drugs \n $25.00";
                UpgradeDrugButton.SetActive(true);
            }
        }
        else if (MoneyHolder.getCountLevel() == 3)
        {
            if (MoneyHolder.getCount() > secondUpgradeCost)
            {
                UpgradeDrugButton.GetComponentInChildren<Text>().text = "Sell Better Drugs \n $50.00";
                UpgradeDrugButton.SetActive(true);
            }
        }
    }

    //================================================================================================================
    //Code for upgrading the Dealers

    private int nextLevelPrice;
    //When an upgrade button is pressed, check the current amount of money and buy/don't buy based on result
    public void nextDealerUpgrade()
    {
        nextLevelPrice = (currDealerLevel * 150 * 5) + 100;
        //Hire A dealer upgrade
        if (MoneyHolder.getCount() >= nextLevelPrice)
        {
            addADealer();
            playRandomSound();
            MoneyHolder.addToCount(-nextLevelPrice);
            currDealerLevel++;
            nextLevelPrice = (currDealerLevel * 150 * 5) + 100;
            UpgradeDealerButton.GetComponentInChildren<Text>().text = "Hire another dealer for $" + (nextLevelPrice / 100.0);
        }
    }

    private void addADealer()
    {
        InvokeRepeating("AutoCount", countTime, countTime);
    }

    //The function that buying the first upgrade starts to repeat.
    public void AutoCount()
    {
        MoneyHolder.addToCount(dealerSellValue);
    }

    //======================================================================================
    //All the manual selling upgrade code

    //Controls which upgrade we're doing
    public void nextSellUpgrade()
    {
        if (MoneyHolder.getCountLevel() == 1)
            FirstUpgrade();
        else if (MoneyHolder.getCountLevel() == 2)
            SecondUpgrade();
        else if (MoneyHolder.getCountLevel() == 3)
            ThirdUpgrade();
    }


    //The 3 upgrades for increasing the amount the player gets per click
    private void FirstUpgrade()
    {
        playBetterDrugSound();
        MoneyHolder.addToCount(-(firstUpgradeCost + 1));
        //Turn on gravity so it falls
        UpgradeDrugButton.GetComponent<Rigidbody2D>().gravityScale = 8;
        //Give it a random rotation
        //First random is left or right rotation, then a random amount of force.
        UpgradeDrugButton.GetComponent<Rigidbody2D>().AddTorque((Random.Range(0, 1) * 2 - 1) * Random.Range(75, 100));
        //UpgradeDrugButton.SetActive(false);
        MoneyHolder.setCountLevel(5);
        //Change the Image of the clicker button
        MoneyHolder.changeImage();
        //Wait 5 seconds before making the button inactive and resetting it.
        StartCoroutine(ResetButton(5));
    }

    //Resets all of the buttons position and rotation stuff.
    IEnumerator ResetButton(float time)
    {
        yield return new WaitForSeconds(time);

        // Code to execute after the delay
        UpgradeDrugButton.SetActive(false);
        UpgradeDrugButton.GetComponent<Rigidbody2D>().gravityScale = 0;
        UpgradeDrugButton.GetComponent<Rigidbody2D>().inertia = 0;
        UpgradeDrugButton.transform.eulerAngles = new Vector3(0, 0, 0);
        //Sets the position of the button back to where it started, relative to anchor
        UpgradeDrugButton.GetComponent<RectTransform>().anchoredPosition = new Vector3(-81, -17, 0);
    }

    private void SecondUpgrade()
    {

        MoneyHolder.addToCount(-(secondUpgradeCost + 1));
        //Turn on gravity so it falls
        UpgradeDrugButton.GetComponent<Rigidbody2D>().gravityScale = 8;
        //Give it a random rotation
        //First random is left or right rotation, then a random amount of force.
        UpgradeDrugButton.GetComponent<Rigidbody2D>().AddTorque((Random.Range(0, 1) * 2 - 1) * Random.Range(75, 100));
        MoneyHolder.setCountLevel(10);
        //Change the Image of the clicker button
        MoneyHolder.changeImage();
        //Wait 5 seconds before making the button inactive and resetting it.
        StartCoroutine(ResetButton(5));
    }

    private void ThirdUpgrade()
    {

        MoneyHolder.addToCount(-(thirdUpgradeCost + 1));
        //Turn on gravity so it falls
        UpgradeDrugButton.GetComponent<Rigidbody2D>().gravityScale = 8;
        //Give it a random rotation
        //First random is left or right rotation, then a random amount of force.
        UpgradeDrugButton.GetComponent<Rigidbody2D>().AddTorque((Random.Range(0, 1) * 2 - 1) * Random.Range(75, 100));
        MoneyHolder.setCountLevel(50);
        //Change the Image of the clicker button
        MoneyHolder.changeImage();
        //Wait 5 seconds before making the button inactive and resetting it.
        StartCoroutine(ResetButton(5));
    }

    //===============================================================================================


    //============================================================================================
    //Sounds

    private void playRandomSound()
    {
        int index = Random.Range(0, dealerSounds.Length);
        audioSource.clip = dealerSounds[index];
        audioSource.Play();
    }

    private void playBetterDrugSound()
    {
        int index = Random.Range(0, betterDrugSounds.Length);
        audioSource.clip = betterDrugSounds[index];
        audioSource.Play();
    }

}
